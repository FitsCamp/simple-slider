{
    class Slider{
        slider = document.querySelector('.items');
        arrowLeft = document.querySelector('.arrow-left');
        arrowRight = document.querySelector('.arrow-right');

        isDown = false;
        startX;
        scrollLeft = 0;
        progress = 0;

        moveLeft = (timestamp, dist, duration) => {
            this.progress -= duration / dist;
            console.log(this.progress, this.scrollLeft);
            if(this.progress > this.scrollLeft){
                this.slider.scroll(this.progress, 0);
            
                requestAnimationFrame(timestamp => {
                    this.moveLeft(timestamp, dist, duration);
                });
            }
        }

        moveRight = (timestamp, dist, duration) => {
            this.progress += duration / dist;
    
            if (this.progress < this.scrollLeft) {
                this.slider.scroll(this.progress, 0);

                requestAnimationFrame(timestamp => {
                    this.moveRight(timestamp, dist, duration);
                });
            }
        }

        arrowEventsStart = () => {
            this.arrowLeft.addEventListener('click', () => {
            this.scrollLeft -= (this.slider.offsetWidth + 16);

            requestAnimationFrame(timestamp => {
                    this.moveLeft(timestamp, 400, 2000);
                }); 
            });
            
            this.arrowRight.addEventListener('click', () => {
                this.scrollLeft += (this.slider.offsetWidth + 16)
            
                requestAnimationFrame(timestamp => {
                    this.moveRight(timestamp, 400, 2000);
                });
            
            });
        }

        slideStart = (event) => {
            this.isDown = true;

            const pageX = this.getPageX(event);

            this.startX = pageX - this.slider.offsetLeft;
            this.scrollLeft = this.slider.scrollLeft;
        }

        getPageX = (event) => {
            if(event.type.includes("mouse")){
                return event.pageX;
            }
            return event.touches[0].pageX;
        }

        slideMove = (event) => {
            if(!this.isDown) return;

            event.preventDefault();
        
            const pageX = this.getPageX(event);
         
            const x = pageX - this.slider.offsetLeft;
            const walk = (x - this.startX) * 1.5;
    
            this.slider.scrollLeft = this.scrollLeft - walk;
        }

        slideEnd = (event) => {
            this.isDown = false;
            let prevDistance = this.slider.offsetWidth;
            let nextDistance = this.slider.offsetWidth/2;

            let prevScrollLeft = this.scrollLeft;
            this.scrollLeft += (this.slider.offsetWidth + 16);
    
            if(this.scrollLeft-this.slider.scrollLeft < nextDistance){
                this.slider.scroll(this.scrollLeft, 0);
            } else {
                console.log(this.scrollLeft-this.slider.scrollLeft);
                if(this.scrollLeft-this.slider.scrollLeft > prevDistance+nextDistance){
                    prevScrollLeft -= (this.slider.offsetWidth + 16);
                    this.slider.scroll(prevScrollLeft, 0);
                } else {
                    this.slider.scroll(prevScrollLeft, 0);
                }
                this.scrollLeft = prevScrollLeft;
            }
            this.progress = this.scrollLeft;
        }

        mobileSlide = () => {
            this.slider.addEventListener('touchstart', this.slideStart);
            this.slider.addEventListener('touchend', this.slideEnd);
            this.slider.addEventListener('touchcancel', this.slideEnd);
            this.slider.addEventListener('touchmove', this.slideMove);
        }

        desktopSlide = () => {
            this.slider.addEventListener('mousedown', this.slideStart);
            this.slider.addEventListener('mouseleave', this.slideEnd);
            this.slider.addEventListener('mouseup', this.slideEnd);
            this.slider.addEventListener('mousemove', this.slideMove);
        }
    }
}
