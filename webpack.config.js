const path = require('path');

module.exports = {
    entry: {
       styles: ['./slider.js'],
    },
    output: {
        path: path.resolve(__dirname, './public/dist/js'),
        filename: 'slider.bundle.js'
    },
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.js?/,
                use: ["babel-loader"]
            },
            {
                test: /\.less/,
                use: ["style-loader", "css-loader", "less-loader"]
            }
        ]
    },
    resolve: {
        modules: ["node_modules"]
    }
};